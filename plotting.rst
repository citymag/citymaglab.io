Data visualization
==================

Below we list all the functions available in the NURI software specifically designed to search for and extract data from a given period of time. More details about each function, with examples on how to execute them, can be found further down this page. A live example can be executed from `this Google Colab tutorial <https://colab.research.google.com/drive/1akhkXln7x-P2FEan-vF1b7cY0y0J0rln>`_.

.. currentmodule:: nuri

.. autosummary::

   plot_spectrogram

Plot spectrogram
----------------

.. autofunction:: plot_spectrogram

