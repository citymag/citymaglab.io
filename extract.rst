Loading sensor data
===================

Below we list all the functions available in the NURI software specifically designed to search for and extract data from a given period of time. More details about each function, with examples on how to execute them, can be found further down this page. A live example can be executed from `this Google Colab tutorial <https://colab.research.google.com/drive/1akhkXln7x-P2FEan-vF1b7cY0y0J0rln>`_.

.. currentmodule:: nuri

.. autosummary::

   get_data
   data_lookup
   check_continuity
   data_offset
   data_extractor
   biomed_converter
   biomed_timing
   read_axis
   get_coord
   bulk_decimate
   get_vmr_data
   get_gnome

Search & Extract
----------------

Retrieve data
`````````````

.. autofunction:: get_data

Data Lookup
```````````

.. autofunction:: data_lookup
		  
Data continuity
```````````````

.. autofunction:: check_continuity

Data offset
```````````

.. autofunction:: data_offset

Biomed sensor data
------------------

Extracting Biomed data
``````````````````````

.. autofunction:: data_extractor

Biomed data conversion
``````````````````````
		  
.. autofunction:: biomed_converter

Time Reading
````````````
		  
.. autofunction:: biomed_timing

Sensor axes reading
```````````````````

.. autofunction:: read_axis

Extract GPS coordinates
```````````````````````
		  
.. autofunction:: get_coord

Decimate bulk of data
`````````````````````
		  
.. autofunction:: bulk_decimate

Twinleaf sensor data
--------------------

.. autofunction:: get_vmr_data

GNOME data
----------
		  
.. autofunction:: get_gnome

