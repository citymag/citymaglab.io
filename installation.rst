Package installation
====================

Working on Google Colab
-----------------------

The following bash script can be used to install the NURI package as well as other dependent packages:

.. code-block:: bash

	git clone https://gitlab.com/citymag/analysis/mlpy.git mlpy
	rm mlpy/mlpy/gsl/gsl.c mlpy/mlpy/liblinear/liblinear.c mlpy/mlpy/libsvm/libsvm.c mlpy/mlpy/adatron/adatron.c
	apt-get install libgsl-dev
	cd mlpy && python setup.py build_ext --include-dirs=/usr/include --rpath=/usr/lib && python setup.py install --single-version-externally-managed --root=/
	rm -rf mlpy
	pip install gwpy obspy matplotlib==3.1.3 scipy==1.5.0
	ln -s /content/nuri/nuri $(python -c "import pip; print(pip.__path__[0].rstrip('/pip'))")/

.. admonition:: MLPY package
   :class: warning

   This software uses the `Machine Learning Python package <http://mlpy.sourceforge.net/>`_, also known as ``mlpy``, to do continuous wavelet transform with complex Morlet wavelets. This package has changed over time and the NURI software may very likely failed to get installed with more recent version of ``mlpy``. Therefore, we have saved a working version in our `Gitlab group <https://gitlab.com/citymag/analysis/mlpy>`_ which can be installed prior to installing NURI.

.. admonition:: Note on SciPy cwt module
   :class: note
   
   While the `cwt module from SciPy <https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.cwt.html>`_ could look like an easier option to do continuous wavelet transform, the module no longer handles complex wavelets as of version 0.18, which is why we are using ``mlpy``.
   

PIP installation
----------------

Stable version
``````````````

The `NURI <https://pypi.org/project/nuri/>`_ Python library can be very easily installed using the Python package manager `PIP <https://pypi.org/>`_ follows:

.. code-block:: bash

   sudo pip install nuri

Development version
```````````````````
   
If you wish to work with the `development version <https://gitlab.com/citymag/analysis/nuri>`_, this can be achieved as follows:

.. code-block:: bash

   sudo pip install -I git+https://gitlab.com/citymag/analysis/nuri.git

Git installation
----------------

We may also want to work on the source code and implement some changes, adding new modules or improving parts of some exisiting functions. To this end, the easiest solution is to clode the Gitlab repository and add the directories in the system PATH. Here's how to do it:

1. Clone the git repository on your local machine.
 
   .. code-block:: bash
		   
      git clone https://gitlab.com/citymag/analysis/nuri.git
     
2. Open your shell config file (e.g. .profile, .bash_profile, .bashrc) and add the following lines.
     
   .. code-block:: bash
		   
      export PATH=$PATH:/path/to/nuri/bin
      export PYTHONPATH=$PYTHONPATH:/path/to/nuri/
     
   The first line will add the location of the self-executing scripts into the system path. The second line includes the path to all the program's modules into the import search path.

