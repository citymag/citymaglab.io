# NURI-related packages
astropy
gwpy
h5py
numpy

# Sphinx-related packages
sphinx
sphinx-material
recommonmark
numpydoc
nbsphinx
sphinx_markdown_tables
sphinx_copybutton
ipython
