.. title:: Docs

.. image:: _images/logo.png
   :target: index.html
   :width: 40%

.. raw:: html

   <br>
	   
.. image:: https://img.shields.io/badge/License-MIT-blue.svg
   :target: https://gitlab.com/citymag/analysis/nuri/-/raw/master/LICENSE.txt
.. image:: https://badge.fury.io/py/nuri.svg
   :target: https://pypi.python.org/pypi/nuri/
.. image:: https://img.shields.io/pypi/dm/nuri
.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.5893774.svg
   :target: https://doi.org/10.5281/zenodo.5893774

The NURI [#f1]_ data analysis software is an open-source `Python <http://www.python.org/>`_ package built to study time-series magnetic field data from arrays of magnetometers covering cities. This is part of the `Urban Magnetometry project <https://citymag.gitlab.io/>`_ led by Prof. Dmitry Budker at the University of California at Berkeley. The lead developer of this software is `Vincent Dumont <https://vincentdumont.gitlab.io/>`_. The source code is available on `GitLab <https://gitlab.com/citymag/analysis/nuri>`_ and everyone is more than welcome to suggest changes and report issues.

How to cite NURI
----------------

Please acknowledge NURI in your publications using the following citation:

.. code-block:: latex

   @misc{nuri_software,
     title        = {{NURI} v1.0.0},
     author       = {Dumont, Vincent},
     doi          = {10.5281/zenodo.5893775},
     url          = {https://doi.org/10.5281/zenodo.5893775},
     howpublished = {[Computer Software] \url{https://doi.org/10.5281/zenodo.5893775}},
     year         = {2022},
     month        = {jan},
   }
   
License Agreement
-----------------

.. literalinclude:: LICENSE.txt
  :language: none
   
Reporting issues
----------------

.. raw:: html

   <br>
   <a href="https://gitlab.com/citymag/analysis/nuri/-/issues" class="button3">
   <font color="#ffffff">Submit a ticket</font>
   </a><br>

If you find any bugs when running this program, please make sure to report them by clicking on the above link and
submit a ticket on the software's official Gitlab repository.

.. rubric:: Footnotes

.. [#f1] The acronym NURI stands for NGA University Research Initiative which is a U.S. grant that we received to support this project.

.. toctree::
   :caption: The Experiment
   :hidden:

   network
   station
   biomed
   twinleaf

.. toctree::
   :caption: The Software
   :hidden:

   installation
   extract
   utilities
   plotting
   fitting
   executable
   
.. toctree::
   :caption: The Results
   :hidden:

   paper.ipynb

