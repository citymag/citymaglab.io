Magnetometer Station
====================

.. _components:

Station components
------------------

Each station was built with the following:

* ASUS X551 laptop (any computer satisfying the system requirements) - 500 USD.
* Biomed eFM3A Fluxgate magnetometer & eMains USB power supply - 3,080 USD
* Garmin 18x LVC GPS (any NMEA GPS with 1 PPS output routed to pin 1, Carrier Detect) - 150 USD
* SerialIO SIO-U232-59 (RS232 to USB converter with +5V on DB9 pin 9)

.. figure:: _images/network_setup.png
   :align: center
   :width: 80%
	   
Laptop specifications
---------------------

The laptops used to stream data for each station from the Biomed data acquisition box to the Google Drive storage are ASUS model X200M touchscreen notebooks. The following table lists some of the specifications:

==================== ========================================
**Operating System** Windows 10 Home
**Processor**        Intel(R) Celeron(R) CPU N2840 @ 2.16GHz
**Memory**           4.00 GB
**System Type**      64-bit
**Storage**          500 GB
==================== ========================================

Windows Issues
``````````````

.. _serial_mouse:
   
Serial Mice detection
~~~~~~~~~~~~~~~~~~~~~

It can happen that, after some time, the USB connection from the GPS receiver starts to malfunction and the mouse cursor behaves erratically, causing the Data Grabber application to close and the upload of the data to stop. The issue has already been encountered in the past and was reported in `this webpage <http://www.taltech.com/support/entry/windows_2000_nt_serial_mice_and_missing_com_port>`__. The problem seems to originate from the Serial Mice which is somehow assumed to be already connected and will therefore not allow the USB connection from the RS232 to USB converter to be recognize and function properly.

.. _windows_update:

Automatic Updates
~~~~~~~~~~~~~~~~~

The automatic update can be removed by following the procedure described in `this page <https://www.forbes.com/sites/gordonkelly/2015/08/26/windows-10-how-to-stop-forced-updates>`_. As central as it is to the core of Windows 10, Windows Update is actually just another Windows process so it can be stopped with these simple steps:

1. Open the Run command (Win + R), in it type: services.msc and press enter
2. From the Services list which appears find the Windows Update service and open it
3. In ‘Startup Type’ (under the ‘General’ tab) change it to ‘Disabled’
4. Restart

Debugging Protocol
``````````````````

Taking all the issues mentioned in the previous section, we have built the following protocol to help debugging quickly any new and ongoing magnetometer station in our network:

1. **Disconnect all the cables:**

   a. If the computer is already on, disconnect all cables and restart
   b. If the computer is off, disconnect all the cables and turn the computer on
       
2. **Check that the Serial Mouse Detection is disabled:**
   
   a. Click on the Windows start button
   b. In the search box, type in ``regedit`` and press the enter key on your keyboard
   c. The Registry editor windows will open
   d. Navigate to the follwoing registry key::
      
	HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\sermouse
      
   e. On the right hand side of the registry editor window you should find subkey named: start
   f. Double click on the "start" subkey and modify the value of the key to: 4 and click the OK button to return to the registry editor window
      
3. **Check that automatic Windows update is disabled:**
   
   a. Open the Run command (Win + R), in it type: services.msc and press enter
   b. From the Services list which appears find the Windows Update service and open it
   c. In "Startup type" (under the "General" tab) change value to ‘Disabled’
   d. In "Startup status" (under the "General" tab) change value to ‘Stop’

4. **Reconnect all the cables:**
   
   a. Turn OFF the computer
   b. While the machine is shut down, re-connect the cables
   c. Turn the computer back ON

5. **Re-open applications:**
      
   a. Open the DataGrabber application and start streaming!
   b. Ensure Teamviewer is open and running


Garmin GPS receiver
-------------------

The GPS receiver used from Garmin has a 1 PPS signal sent through an RS-232 connection which can be transfered to the computer via a RS-232 to USB converter. More information can be found `in the manual <_materials/docs/GPS_18x_Tech_Specs.pdf>`_.

Network Time Protocol
`````````````````````

The `Network Time Protocol <https://en.wikipedia.org/wiki/Network_Time_Protocol>`_ (NTP) can be used to keep the computers in accurate time. `David and Cecilia Taylor's website <http://www.satsignal.eu/>`_ provides very detailed information on how to set up NTP on a Windows machine. The actual NTP software can be downloaded from the `Meinberg Global <https://www.meinbergglobal.com/>`_ website as explained in the aforementioned tutorial. Below we provided the relevant pages:

- `Setting up NTP on Windows <_materials/docs/ntp_windows_setup.pdf>`_ (`HTML <http://www.satsignal.eu/ntp/setup.html>`__)
- `Meinberg NTP Software Downloads <_materials/docs/ntp_meinberg_downloads.pdf>`_ (`HTML <https://www.meinbergglobal.com/english/sw/ntp.htm#ntp_stable>`__)
- `NTP on Vista and Windows-7_8 <_materials/docs/ntp_windows_vista.pdf>`_ (`HTML <http://www.satsignal.eu/ntp/NTP-on-Windows-Vista.html>`__)
- `Using MRTG to monitor NTP <_materials/docs/ntp_mrtg.pdf>`_ (`HTML <http://www.satsignal.eu/ntp/NTPandMRTG.html>`__)

RS-232 to USB connection
````````````````````````

To make the NTP work the PPS signal has to go to Pin 1 (Carrier Detect).

Read GPS timing
```````````````

A fairly small script can be written in order to read the PPS signal out of the GPS receiver through the RS232-to-USB converter. In order to work though, on needs to install the Virtual Instrument Software Architecture (VISA) from National Instruments `here <http://www.ni.com/download/ni-visa-14.0.2/5075/en/>`__ and the `PyVISA <https://pyvisa.readthedocs.io/en/stable/>`_ Python library.
