TwinLeaf sensors
================

Vector Magnetometer
-------------------
      
.. image:: https://twinleaf.com/vector/VMR/VMR.jpeg
   :align: center
   :width: 70%

We implemented two new sensors from TwinLeaf LLC (see picture above) in our network. Those are vector magnetometers which have a sensitivity down to the picotesla level. The two great advantages of those sensors are that they are very small, so easy to transport, and can be directly connected to the station’s laptop via USB therefore avoiding the need of a data acquisition box, like for the originally Biomed sensors used. The data acquisition software had to be modified to properly incorporate those magnetometers which was not technically straightforward as the software was not originally built to add new type of magnetic field sensors.

Manual & LabView Program
------------------------

`The manual <_materials/docs/VM4_Manual.pdf>`_ is available for more information. The sensor can also be tested by using the ``VM Monitor.vi`` application provided by Dave Newby from TwinLeaf and available `in this compressed zip repository <_materials/files/VM_nanomsg-labview_2014.zip>`_.

.. image:: https://github.com/twinleaf/tio-labview/raw/master/doc/VMmonitor.gif
   :align: center
   :width: 50%
