Biomed sensor data
==================

The data from the Biomed sensors are acquired using the `UrbanMagnetometer <https://gitlab.com/citymag/acquisition/UrbanMagnetometer>`_ software built by Lena Zhivun. However,

Characteristics
---------------

eFM-3A Fluxgate Sensor
``````````````````````

The following information was taken from the online description of the instrument in the Biomed Jena website. The webpage no longer exists but a cache version is still available in the `web.archive.org <http://web.archive.org/web/20150210134615/http://biomed-jena.de/index.php/efm3a-fluxgate-magnetometer.html>`_ website.

.. image:: _images/snapshot.png

.. note::
  
   `Click here <_static/2018_eFM-3A_Rev03_eng.pdf>`__ to access the data sheet for the new version of the eFM-3A sensor with different characteristics.

eMAINS Power Supply
```````````````````

.. image:: _images/eMains_datasheet.png
   :target: _static/eMains_datasheet.pdf
   :width: 50%

Measured inside shield
----------------------

The Biomed magnetometer sensitivity was estimated based on the readings of the NURI-station-01 sensor inside the magnetic shields (`click here <https://drive.google.com/drive/folders/0B2nMpmvMHAr-MV9tMnUzU0pWUVE>`__ to access the data), which has been recording continuously in Birge 221 inside a 2-layer mu-metal magnetic shields.

- The magnetometer sampling rate is ~4 kHz, so the bandwidth is ~2 kHz.
- Digitization noise is 2 nT peak-to-peak in X direction
- Maximal noise is observed along the Z direction and is 10 nT peak-to-peak

Measured digitization-noise-limited sensitivity is then (assuming flat spectrum):

- Single axis: 2nT/sqrt(2000Hz) ~ 45 pT/sqrt(Hz) p-p
- Magnitude: sqrt(3) * 45 ~ 78 pT/sqrt(Hz) p-p

Power consumption
-----------------

The magnetometer can be converted to work from batteries is the constant voltage is applied to the inputs of the linear voltage stabilizers. The permitted input voltage levels are ±20-35 V (assuming the linear voltage stabilizers are LM7818 and LM7819). The linear voltage stabilizers then will output ±18V to power the magnetic sensor. When a ~26V source is connected, the current consumption on the positive rail is 96 mA, on the negative rail it’s 64 mA.

Data Structure
--------------
  
The data arrives from Biomed sensors in chunks of ~300-1000 points (see diagram below). Each sensor has a single ADC that switches between X, Y, and Z channels at three times the sampling rate. Upon arrival the data from three channels is separated into raw_x, raw_y, and raw_z files, correspondingly.

.. image:: _images/data_structure.png
   :class: no-scaled-link
   :align: center
   :width: 100%

Magnetic field data
-------------------

The raw_x, raw_y and raw_z files are arrays of double precision floating point, separated in time by the sampling period. The X, Y, Z values with the same array index correspond to X, Y, Z components of a single vector field measurement. Because of how the device works, the vector field components are not recorded at exactly the same time. All field values are in microtesla.

Timing information
------------------

The time binary files store the arrival time information for every packet of data received by the data acquisition software. Each packet contains 138 data points sampled at 3960 Hz rate. Therefore, the average time length of a packet, or :math:`\Delta t`, is:

.. math:: \Delta t = 138 / 3960 \approx 0.034848\,\mathrm{s} \approx 35\,\mathrm{ms}

Below we describe the two versions of file structure used to store the timing information. The system was improved in June 2016 after implementation of a `timing correction <timing_correction.html>`_ algorithm to fix the timing delay in packet arrival.

.. warning:: We found out that the date and time at which the computer is set, i.e. date and time as displayed in the taskbar's clock, matters. Indeed, the timestamps read directly from the timing binary file shows an offset from the expected recorded value equal to the time difference between UTC and PST (Pacific Standard Time) time zones. The problem was fixed on **Thursday 7 November 2017** by fixing the date time on all computers to the UTC time zones.

Version 1
`````````

*Effective until June 7, 2016, 7AM UTC.*

The time data files describes when each chunk of data was acquired. The filename corresponds to the local GPS date and hour of the first chunk. The binary file is structured as an array of records describing the sequence of chunks as they arrive. Each chunk description has the following structure with fields in this order (28 bytes per record):

.. code-block:: c
		
  int64_t start;		// index of the chunk data start in X, Y, Z arrays
  int32_t length; 		// number of points in the chunk in X, Y, Z arrays
  int64_t timestamp;	 	// GPS-disciplined Windows timestamp (15 ms resolution)
  double performance_counter;   // System oscillator seconds (<1 us resolution, no GPS)

- The ``start`` field is the index of where the chunk data begins in raw_x, raw_y and raw_z files (so 8*start is the offset in bytes of the chunk start in each file).
- ``length`` is the number of sequential values in each X, Y, and Z arrays that arrived within this chunk.
- ``timestamp`` is the GPS-disciplined system time recorded at the time of the chunk arrival (`DateTime.ToBinary() <https://msdn.microsoft.com/en-us/library/system.datetime.tobinary(v=vs.110).aspx>`_ in Pacific time zone, see `how to convert .NET DateTime to other formats <https://en.code-bude.net/2013/09/22/how-to-convert-csharp-datetime-ticks-into-unix-timestamp/>`_). The Windows' native timestamp format, also called ticks, is a signed 64-bit integer representing :math:`10^7` times the number of seconds since midnight UTC on January 1st, Year 1 on the proleptic Gregorian calendar.
- Since Windows only updates the system time once in 15 ms, ``performance_counter`` is also recorded upon the data arrival. It stores the number of seconds measured by the system oscillator, which has <1 us resolution and is better suited for measuring short time intervals. performance_counter is NOT synchronized with the GPS clock. 

Version 2
`````````

*Effective from June 11, 2016, 3AM UTC.*

The time data files describes when each chunk of data was acquired. The filename corresponds to the UTC date and hour of the first chunk. It is structured as an array of records describing the sequence of chunks as they arrive. Each chunk description has the following structure with fields in this order (63 bytes per record):

.. code-block:: c

  int64_t start;		// index of the chunk data start in X, Y, Z arrays
  int32_t length; 		// number of points in the chunk in X, Y, Z arrays
  byte valid;			// 1 if time is valid, 0 otherwise
  int64_t ticks;	 	// performance counter value in ticks value (400 ns resolution)
  double timestamp;	 	// interpolated Unix timestamp (UTC)
  double latitude;	 	// GPS latitude
  char ew;			// ASCII “E” if East, “W” if West
  double longitude;	 	// GPS longitude
  char ns;			// ASCII “S” if South, “N” if North
  double speed_knots;	 	// GPS speed in knots
  double angle_degrees; 	// GPS heading in degrees

- The ``start`` field is the index of where the chunk data begins in raw_x, raw_y and raw_z files (so 8*start is the offset in bytes of the chunk start in each file).
- ``length`` is the number of sequential values in each X, Y, and Z arrays that arrived within this chunk.
- ``valid`` is set to 1 when enough data is available to interpolate the GPS time, and 0 when it’s not (for example, if GPS receiver hasn’t sent any data in the last several minutes).
- ``ticks`` is the value of the performance counter of the system (ticks since the system start). Typical counter frequency for the sensor stations is 2533200 Hz (or 400 ns).
- ``timestamp`` is the interpolated GPS time stamp recorded at the time of the chunk arrival recorded as Unix time in UTC timezone.
- ``latitude`` and ``longitude`` are the sensor coordinates recorded as a floating point number. For example, 12311.12 translates into 123 degrees 11.12 minutes.
- ``speed_knots`` is the speed of the sensor in knots, and ``angle_degrees`` is the heading of the sensor in degrees with respect to the north. The coordinates, speed, and heading are updated once per second and are not interpolated. 

