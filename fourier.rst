Time Series Decomposition
=========================

The video below provides a rather elegant introduction to fourier transform analysis. In particular, it demonstrates how wrapping the time series inside a 2π circle can be used to decompose the series into individual sine wave functions, which can be written in a complex exponential form using Euler's formula.

.. raw:: html

	 <iframe width="560" height="315" src="https://www.youtube.com/embed/spUNpyF58BY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
