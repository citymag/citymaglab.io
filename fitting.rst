Fitting distributions
=====================

.. currentmodule:: nuri

.. autofunction:: gaussian_fit

Gaussian fitting
----------------

.. currentmodule:: nuri.analysis.fitting
.. autoclass:: GaussianFit
   :no-inherited-members:

     None

   .. rubric:: Methods Summary

   .. autosummary::

      ~__init__
      ~gauss_single
      ~gauss_fct
      ~gauss_fit
      ~get_params
      ~get_fit
      
   .. rubric:: Methods Documentation

   .. automethod:: __init__
   .. automethod:: gauss_single
   .. automethod:: gauss_fct
   .. automethod:: gauss_fit
   .. automethod:: get_params
   .. automethod:: get_fit

Skewed Gaussian fitting
-----------------------

.. currentmodule:: nuri.analysis.fitting
.. autoclass:: SkewedGaussianFit
   :no-inherited-members:

     None

   .. rubric:: Methods Summary

   .. autosummary::

      ~__init__
      ~skew_single
      ~skew_fct
      ~skew_fit
      ~get_params
      ~get_fit
      
   .. rubric:: Methods Documentation

   .. automethod:: __init__
   .. automethod:: skew_single
   .. automethod:: skew_fct
   .. automethod:: skew_fit
   .. automethod:: get_params
   .. automethod:: get_fit

Independent Skewed Gaussian
---------------------------

.. currentmodule:: nuri.analysis.fitting
.. autoclass:: IndSkewedGaussian
   :no-inherited-members:

     None

   .. rubric:: Methods Summary

   .. autosummary::

      ~__init__
      ~skew_single
      ~skew_fct
      ~get_fit
      
   .. rubric:: Methods Documentation

   .. automethod:: __init__
   .. automethod:: skew_single
   .. automethod:: skew_fct
   .. automethod:: get_fit

Mixed Gaussian fitting
----------------------

.. currentmodule:: nuri.analysis.fitting
.. autoclass:: MixedGaussianFit
   :no-inherited-members:

     None

   .. rubric:: Methods Summary

   .. autosummary::

      ~__init__
      ~gauss_fct
      ~skew_fct
      ~mix_fct
      ~mix_fit
      ~get_fit
      

   .. rubric:: Methods Documentation

   .. automethod:: __init__
   .. automethod:: gauss_fct
   .. automethod:: skew_fct
   .. automethod:: mix_fct
   .. automethod:: mix_fit
   .. automethod:: get_fit

