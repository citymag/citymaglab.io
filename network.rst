Urban sensor network
====================

Berkeley deployment
-------------------

Below we show the original location of the 4 magnetometers in Berkeley. The thick black line represents the path of the Bay Area Rapid Transit (BART) transportation system.

.. image:: _images/map.png
   :width: 100%

Deployment in Brooklyn
----------------------
	   
On October 2017, the network started to deployed in New York City at the `Center for Urban Science and Progress <http://cusp.nyu.edu/>`_ (CUSP) in downtown Brooklyn. Only one station (NURI-STATION-03) was made available at that time as the second station that was brought had its Biomed data acquisition box not working. Two working stations have then been brought to CUSP in December 2017.

