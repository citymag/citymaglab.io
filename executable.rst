Executable version
==================

An executable, ``nuri``, can be called to execute some standard operations listed below:

.. list-table::
   :widths: 30 70
   :header-rows: 1

   * - Operation
     - Description
   * - ``activity``
     - Plot the activity of the network
   * - ``download``
     - Download data from the Google server
   * - ``extract``
     - Extract data
   * - ``convert``
     - Convert magnetic field data files
   * - ``wavelet``
     - Plot wavelet transform
   * - ``psd``
     - Plot power spectral density
   * - ``availability``
     - Check data availability

Below is the help message can be viewed after typing ``nuri --help`` on the terminal:

.. code-block:: none

    usage: nuri [-h] [--date] [--delay] [--dest] [--gps] [--length] [--median]
                [--mmin] [--mmax] [--offset] [--output] [--overlap] [--path]
                [--period ] [--rate] [--sensor] [--station] [--trange ] [--tunit]
                [--ufactor]
                {activity,download,extract,convert,wavelet,psd,availability}
    
    Sensor Network Time-frequency analysis tool.
    
    positional arguments:
      {activity,download,extract,convert,wavelet,psd,availability}
                            Operation to be run
    
    optional arguments:
      -h, --help            show this help message and exit
      --date                Starting date (format: YYYY-MM-HH)
      --delay               Timestamp delay in seconds
      --dest                Custom destination repository
      --gps                 Where we consider GPS timing
      --length              Time length (in seconds)
      --median              Divide time series by median value
      --mmin                Minimum magnetic field
      --mmax                Maximum magnetic field
      --offset              Timing offset
      --output              Output file name
      --overlap             Period to overlap data (in seconds)
      --path                Path to data file or repository
      --period              Time period (format: YYYY-MM-HH)
      --rate                Targeted sampling rate (default: None)
      --sensor              Sensor type
      --station             Station index number
      --trange              Time range
      --tunit               Time frame unit
      --ufactor             Factor from local to uT   

